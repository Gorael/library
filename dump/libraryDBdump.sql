CREATE DATABASE  IF NOT EXISTS `libraryDB` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `libraryDB`;
-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: libraryDB
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `isbn` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (1,'Гарри Поттер и Дары Смерти','Джоан Роулинг','Очень хорошая книга','2007-07-21','9780545010221'),(2,'Властелин колец','Джон Рональд Руэл Толкин','Кольцо','1954-07-29','9780007117116'),(3,'Игра престолов','Джордж Рэймонд Ричард Мартин','Драконы и белые ходоки','1996-08-01','9788496422612'),(4,'451 градус по Фаренгейту','Рэй Брэдбери','Утопия','1953-10-01','9788952708922'),(5,'Анна Каренина','Лев Николаевич Толстой','-','1877-10-01','9784334751630'),(6,'Цветы для Элджернона','Дэниел Киз','-','1959-04-01','9780575400207'),(7,'Мертвая зона','Стивен Кинг','Противостояние','1979-08-01','9783453432727'),(8,'На берегу','Иэн Макьюэн','Романтика и тд','2007-01-01','9785699317493'),(9,'Никогде','Нил Гейман','Монстры и святые','1996-09-16','9783641038649'),(10,'Понаехавшая','Наринэ Абгарян','Покорение Москвы','2011-01-01','9785226043758'),(11,'Чемодан','Сергей Довлатов','Юмор','1986-01-02','9780802112460');
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'libraryDB'
--

--
-- Dumping routines for database 'libraryDB'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-20 16:59:53
