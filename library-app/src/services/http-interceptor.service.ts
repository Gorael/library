import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs, Request, RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

/**
 * Устанавливает для всех http запросов нужные headers
 */
@Injectable()
export class HttpInterceptor {

  public options;

  constructor(private http: Http) { }

  request(url: string | Request, options?: RequestOptionsArgs) {
    return this.intercept(this.http.request(url, this.options));
  }

  get(url: string, options?: RequestOptionsArgs) {
    return this.intercept(this.http.get(url, this.options));
  }

  post(url: string, body, options?: RequestOptionsArgs) {
    return this.intercept(this.http.post(url, body, this.getRequestOptionArgs(this.options)));
  }

  put(url: string, body: string, options?: RequestOptionsArgs) {
    return this.intercept(this.http.put(url, body, this.getRequestOptionArgs(this.options)));
  }

  delete(url: string, options?: RequestOptionsArgs) {
    return this.intercept(this.http.delete(url, this.options));
  }

  getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
    if (options == null) {
      options = new RequestOptions();
    }
    if (options.headers == null) {
      options.headers = new Headers();
      options.headers.append('Content-Type', 'application/json');
      options.withCredentials = true;
    }
    return options;
  }

  intercept(observable: Observable<any>) {
    return observable.map((res: Response) => {
      return res.json()
    }).catch((err, source) => {
      let errors: Array<Error> = err.json()
      switch (err.status) {
        case 403:
          return Observable.throw(errors);
        case 400:
          return Observable.throw(errors)
      }

      return Observable.throw(errors);

    });


  }
}

