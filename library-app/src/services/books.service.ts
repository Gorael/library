
import { Injectable, isDevMode } from '@angular/core';
import {HttpService} from "./http.service";
import {Observable} from "rxjs/Observable";
import {Book} from "../models/Book";

@Injectable()
export class BooksService {

  constructor(private httpService: HttpService) {
  }

// <td>ID <div class="sort" (click)="setBookTarget(0)">sort</div></td>
// <td>Назание <div class="sort" (click)="setBookTarget(1)">sort</div></td>
// <td>Дата <div class="sort" (click)="setBookTarget(2)">sort</div></td>
// <td>Автор <div class="sort" (click)="setBookTarget(3)">sort</div></td>
// <td>ISBN <div class="sort" (click)="setBookTarget(4)">sort</div></td>

  getBooks(bookTarget):Observable<any>{
    let orderBy;
    let desc;
    if (bookTarget[1]>0){
      orderBy='name';
      if (bookTarget[1]>1){
        desc=true;
      }else desc=false;
    }else if (bookTarget[2]>0){
      orderBy='date';
      if (bookTarget[2]>1){
        desc=true;
      }else desc=false;
    } else if (bookTarget[3]>0){
      orderBy='author';
      if (bookTarget[3]>1){
        desc=true;
      }else desc=false;
    } else if (bookTarget[4]>0){
      orderBy='isbn';
      if (bookTarget[4]>1){
        desc=true;
      }else desc=false;
    }else {
      orderBy='id';
      if (bookTarget[0]>1){
        desc=true;
      }else desc=false;
    }
    return this.httpService.books(orderBy,desc);
  }

  getBook(id:number):Observable<any>{
    return this.httpService.book(id);
  }

  addNewBook(book:Book):Observable<any>{
    return this.httpService.newBook(book);
  }

}
