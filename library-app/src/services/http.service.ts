import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import {HttpInterceptor} from "./http-interceptor.service";
import {Book} from "../models/Book";

@Injectable()
export class HttpService {

  apiBaseUrl: string = '';

  constructor(private httpInterceptor: HttpInterceptor) {
    this.apiBaseUrl = "http://localhost:8084/api";
  }


  public newBook(book:Book){
    return this.httpInterceptor.post(this.apiBaseUrl+'/newbook',{name:book.name,author:book.author,
      date:book.date,description:book.description,isbn:book.isbn});;
  }

  public books(orderBy,desc){
    return this.httpInterceptor.post(this.apiBaseUrl+'/books',{orderBy:orderBy,desc:desc});
  }

  public book(id:number):Observable<any>{
    return this.httpInterceptor.get(this.apiBaseUrl+'/book/'+id);
  }

}
