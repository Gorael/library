export class Book{
  id:number;
  name:string;
  date:string;
  author:string;
  isbn:string;
  description:string;
}
