import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {BooksComponent} from "../components/books/books.component";
import {BookDetailComponent} from "../components/book-detail/book-detail.component";
import {NewBookComponent} from "../components/new-book/new-book.component";


const routes:Routes=[
  {
    path: '',
    component: BooksComponent
  },{
    path: 'book/:id',
    component: BookDetailComponent
  },{
    path: 'newbook',
    component: NewBookComponent
  }
  ];

@NgModule({
  imports:[RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule {}
