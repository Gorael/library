import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule}   from '@angular/forms';
import { AppComponent } from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {BooksComponent} from "../components/books/books.component";
import {HttpInterceptor} from "../services/http-interceptor.service";
import {HttpService} from "../services/http.service";
import {BooksService} from "../services/books.service";
import {HttpModule} from "@angular/http";
import {PagerService} from "services/pager.service";
import {BookDetailComponent} from "../components/book-detail/book-detail.component";
import {NewBookComponent} from "../components/new-book/new-book.component";


@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    BookDetailComponent,
    NewBookComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [HttpInterceptor, HttpService, BooksService,PagerService],
  bootstrap: [AppComponent]
})

export class AppModule { }
