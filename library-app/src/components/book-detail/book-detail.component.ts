import { Component, OnInit,ViewChild } from '@angular/core';
import {BooksService} from "../../services/books.service";
import {PagerService} from "../../services/pager.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Book} from "../../models/Book";

@Component({
  selector: 'book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.less']
})
export class BookDetailComponent implements OnInit {

  book:Book=new Book();

  constructor(private booksService:BooksService, private route:ActivatedRoute, private router:Router) {}

  ngOnInit(){
    this.route.params
      .switchMap((params: Params) => this.booksService.getBook(+params['id']))
      .subscribe(book => {
        this.book = book;
        console.log(book);
      });
  }
  toMain(){
    this.router.navigate(['']);

  }


}
