import { Component, OnInit,ViewChild } from '@angular/core';
import {BooksService} from "../../services/books.service";
import {PagerService} from "../../services/pager.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Book} from "../../models/Book";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'new-book',
  templateUrl: './new-book.component.html',
  styleUrls: ['./new-book.component.less']
})
export class NewBookComponent implements OnInit {

  book:Book;
  answer:boolean=false;

  constructor(private booksService:BooksService, private route:ActivatedRoute,
              private router:Router) {  }

  ngOnInit(){

  }
  toMain(){
    this.router.navigate(['']);
  }


  onSubmit(form: any): void {
    this.book=form;
    this.booksService.addNewBook(this.book).subscribe((answer)=>this.answer=answer);
  }


}
