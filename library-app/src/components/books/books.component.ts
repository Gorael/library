import { Component, OnInit,ViewChild } from '@angular/core';
// import { DataTable, DataTableTranslations, DataTableResource } from 'angular-2-data-table';
import {BooksService} from "../../services/books.service";
import {PagerService} from "../../services/pager.service";
import {Router} from "@angular/router";

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.less']
})
export class BooksComponent implements OnInit {

  books = [];
  //  [id:0,name:0,date:0,author:0,isbn:0];
  bookTarget=[0,0,0,0,0];

  pager: any = {};

  pagedItems: any[];

  constructor(private booksService:BooksService,private pagerService: PagerService, private router:Router) {}

  ngOnInit(){
     this.dowloadBooks();
  }

  clearTarget(){
    for(let i=0;i<this.bookTarget.length;i++){
      this.bookTarget[i]=0;
    }
  }

  dowloadBooks(){
    this.booksService.getBooks(this.bookTarget).subscribe((books)=>{
      this.books=books;
      this.setPage(1);
    });
  }

  setBookTarget(index){
    let temp=this.bookTarget[index];
    this.clearTarget();
    if (temp<2) {
      temp++;
      this.bookTarget[index]=temp;
    }
    this.dowloadBooks();
  }
  setPage(page) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.books.length, page);

    this.pagedItems = this.books.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  selectBook(id){
    this.router.navigate(['/book',id]);
  }

  toNewBook(){
    this.router.navigate(['/newbook']);
  }

}
