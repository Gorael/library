package LibraryServer.dao.exceptions;

/**
 *
 * @author alexey
 */
public class ApiException extends Exception {

    protected Class<?> controllerClass;
    protected String logMessage;

    public ApiException(String message, Class<?> controllerClass, String logMessage) {
        super(message);
        this.controllerClass = controllerClass;
        this.logMessage = logMessage;
    }
    
    public ApiException(String message){
        super(message);
    }

    public Class<?> getControllerClass() {
        return controllerClass;
    }

    public String getLogMessage() {
        return logMessage;
    }
}