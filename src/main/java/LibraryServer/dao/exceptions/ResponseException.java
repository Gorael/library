package LibraryServer.dao.exceptions;

/**
 *
 * @author alexey
 */
public class ResponseException extends ApiException{
    
    
    
    public ResponseException(String message, Class<?> controllerClass, String logMessage){
        super(message,controllerClass,logMessage);
    }

    public ResponseException(String message) {
        super(message);
    }
}
