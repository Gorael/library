package LibraryServer.dao.mybatis.mappers;

import LibraryServer.models.Book;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;


@Mapper
public interface LibraryMapper {

    @Select("SELECT id,name,date,author,isbn from books")
    List<Book> getBooksOrderId();

    @Select("SELECT id,name,date,author,isbn from books order by id desc")
    List<Book> getBooksOrderIdDesc();

    @Select("SELECT id,name,date,author,isbn from books order by name")
    List<Book> getBooksOrderName();

    @Select("SELECT id,name,date,author,isbn from books order by name desc")
    List<Book> getBooksOrderNameDesc();

    @Select("SELECT id,name,date,author,isbn from books order by date")
    List<Book> getBooksOrderDate();

    @Select("SELECT id,name,date,author,isbn from books order by date desc")
    List<Book> getBooksOrderDateDesc();

    @Select("SELECT id,name,date,author,isbn from books order by author")
    List<Book> getBooksOrderAuthor();

    @Select("SELECT id,name,date,author,isbn from books order by author desc")
    List<Book> getBooksOrderAuthorDesc();

    @Select("SELECT id,name,date,author,isbn from books order by isbn")
    List<Book> getBooksOrderISBN();

    @Select("SELECT id,name,date,author,isbn from books order by isbn desc")
    List<Book> getBooksOrderISBNDesc();

    @Select("SELECT id,name,date,author,isbn,description from books where id=#{id};")
    Book getBook(@Param("id") Integer id);

    @Insert("Insert into books (name,date,author,isbn,description) values(#{book.name},#{book.date},#{book.author},#{book.isbn},#{book.description});")
    Boolean addNewBook(@Param("book") Book book);
}
