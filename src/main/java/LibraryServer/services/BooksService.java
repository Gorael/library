package LibraryServer.services;

import LibraryServer.dao.mybatis.mappers.LibraryMapper;
import LibraryServer.models.Book;
import LibraryServer.models.BooksOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class BooksService {

    @Autowired
    LibraryMapper libraryMapper;

    public List<Book> getBooks(BooksOrder booksOrder){
        List<Book> answer=new ArrayList<>();
        switch(booksOrder.getOrderBy()){
            case "id":{
                if (booksOrder.getDesc()){
                    answer=libraryMapper.getBooksOrderIdDesc();
                }else{
                    answer=libraryMapper.getBooksOrderId();
                }
                break;
            }
            case "name":{
                if (booksOrder.getDesc()){
                    answer=libraryMapper.getBooksOrderNameDesc();
                }else{
                    answer=libraryMapper.getBooksOrderName();
                }
                break;
            }
            case "author":{
                if (booksOrder.getDesc()){
                    answer=libraryMapper.getBooksOrderAuthorDesc();
                }else{
                    answer=libraryMapper.getBooksOrderAuthor();
                }
                break;
            }
            case "isbn":{
                if (booksOrder.getDesc()){
                    answer=libraryMapper.getBooksOrderISBNDesc();
                }else{
                    answer=libraryMapper.getBooksOrderISBN();
                }
                break;
            }
            case "date":{
                if (booksOrder.getDesc()){
                    answer=libraryMapper.getBooksOrderDateDesc();
                }else{
                    answer=libraryMapper.getBooksOrderDate();
                }
                break;
            }
        }

        return answer;
    }

    public Book getBook(Integer id){
        return libraryMapper.getBook(id);
    }

    public Boolean addNewBook(Book book){
        return libraryMapper.addNewBook(book);
    }
}
