package LibraryServer.controllers;

import LibraryServer.models.Book;
import LibraryServer.models.BooksOrder;
import LibraryServer.services.BooksService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Path;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class BooksController {

    @Autowired
    BooksService booksService;

    @RequestMapping(method = RequestMethod.POST,
            value = "/books",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    public List<Book> getBooks(@RequestBody BooksOrder booksOrder)  {
        return booksService.getBooks(booksOrder);
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/book/{bookID}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Book getBook(@PathVariable("bookID") Integer id)  {
        return booksService.getBook(id);
    }

    @RequestMapping(method = RequestMethod.POST,
            value = "/newbook",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Boolean addNewBook(@RequestBody Book book)  {
        return booksService.addNewBook(book);
    }

}
